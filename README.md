# Kubernetes

## What is Kubernetes?

## Why we use kubernetes?

## Features of kubernetes?

## Kubernetes Architecture?

## Pod Lifecycle

## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects